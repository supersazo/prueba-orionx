import { useState, useEffect } from "react";
import { FormattedMessage, useIntl } from 'react-intl';

function ContactForm() {
  const intl = useIntl();

  const initialValues = {
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    message: ""
  };

  const initialErrors = {
    formRequired: true
  }

  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState(initialErrors);
  const [formSubmit, setFormSubmit] = useState();
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleValidation = () => {
    setFormErrors(validate(formValues));
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));

    if (Object.keys(formErrors).length === 0 && isSubmit) {
      fetch("https://orionx.requestcatcher.com/", {
        method: "POST",
        mode: "no-cors",
        body: JSON.stringify({
          firstname: formValues.firstname,
          lastname: formValues.lastname,
          email: formValues.email,
          phone: '+' + formValues.phone,
          message: formValues.message
        }),
      });
      setFormSubmit("Datos Enviados!");
      setFormValues(initialValues);
      setFormErrors(initialErrors);
      setIsSubmit(false);
    }
  };

  const handlePhoneNumber = (e) => {
    const phoneNumber = (e.target.validity.valid) ? e.target.value : formValues.phone;
    setFormValues({...formValues, phone: phoneNumber});
  }

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 ) {
      setIsSubmit(true);
    } else {
      setIsSubmit(false);
    }
  }, [setIsSubmit, formErrors, formValues]);

  const validate = (values) => {
    const errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    const pattern = new RegExp(/^[0-9\b]+$/);
    
    if (!values.firstname) {
      errors.firstname = "form.name-required";
    }
    if (!values.lastname) {
      errors.lastname = "form.lastname-required";
    }
    if (!values.email) {
      errors.email = "form.email-required";
    } else if (!regex.test(values.email)) {
      errors.email = "form.email-invalid";
    }
    if (!values.phone) {
      errors.phone = "form.phone-required";
    } else if (!pattern.test(values.phone)) {
      errors.phone = "form.phone-only-numbers";
    }else if(values.phone.length !== 11){
      errors.phone = "form.phone-invalid";
    }
    
    return errors;
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="px-3 py-3 mb-40 sm:p-6">
        <div className="grid grid-cols-6 gap-6 max-w-2xl m-auto">
          <div className="col-span-6 sm:col-span-3">
            <label htmlFor="firstname" className="text-sm font-medium text-gray-700">
              <FormattedMessage id="form.name" />
            </label>
            <input
              className={`h-11 mt-1 p-2 w-full border rounded-xl sm:text-sm ${formErrors.firstname ? "border-red-200 outline-red-600 focus-visible:border-red-600":"border-neutral-200 focus-visible:border-blue-600"}`}
              type="text"
              name="firstname"
              placeholder={intl.formatMessage({ id: "form.name-placeholder" })}
              value={formValues.firstname}
              onChange={handleChange}
              onKeyUp={handleValidation}
            />
            {formErrors.firstname &&
              <p className="text-xs text-red-600">
                <FormattedMessage id={formErrors.firstname} />
              </p>
            }
          </div>

          <div className="col-span-6 sm:col-span-3">
            <label htmlFor="lastname" className="block text-sm font-medium text-gray-700">
              <FormattedMessage id="form.lastname" />
            </label>
            <input
              className={`h-11 mt-1 p-2 w-full border rounded-xl sm:text-sm ${formErrors.lastname ? "border-red-200 outline-red-600 focus-visible:border-red-600":"border-neutral-200 focus-visible:border-blue-600"}`}
              type="text"
              name="lastname"
              placeholder={intl.formatMessage({ id: "form.lastname-placeholder" })}
              value={formValues.lastname}
              onChange={handleChange}
              onKeyUp={handleValidation}
            />
            {formErrors.lastname &&
              <p className="text-xs text-red-600">
                <FormattedMessage id={formErrors.lastname} />
              </p>
            }
          </div>

          <div className="col-span-6 sm:col-span-3">
            <label htmlFor="email" className="block text-sm font-medium text-gray-700">
              <FormattedMessage id="form.email" />
            </label>
            <input
              className={`h-11 mt-1 p-2 w-full border rounded-xl sm:text-sm ${formErrors.email ? "border-red-200 outline-red-600 focus-visible:border-red-600":"border-neutral-200 focus-visible:border-blue-600"}`}
              type="text"
              name="email"
              placeholder={intl.formatMessage({ id: "form.email-placeholder" })}
              value={formValues.email}
              onChange={handleChange}
              onKeyUp={handleValidation}
            />
            {formErrors.email &&
              <p className="text-xs text-red-600">
                <FormattedMessage id={formErrors.email} />
              </p>
            }
          </div>
          
          <div className="col-span-6 sm:col-span-3">
            <label htmlFor="phone" className="block text-sm font-medium text-gray-700">
              <FormattedMessage id="form.phone" />
            </label>
            <div className="relative rounded-md shadow-sm">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 text-lg"> + </span>
              </div>
              <input
                className={`h-11 mt-1 p-2 pl-7 w-full border rounded-xl appearance-none sm:text-sm ${formErrors.phone ? "border-red-200 outline-red-600 focus-visible:border-red-600":"border-neutral-200 focus-visible:border-blue-600"}`}
                type="tel"
                pattern="[0-9]*"
                name="phone"
                maxLength="11"
                placeholder="56998765432" 
                value={formValues.phone}
                onInput={handlePhoneNumber}
                onKeyUp={handleValidation}
              />
            </div>
            {formErrors.phone &&
              <p className="text-xs text-red-600">
                <FormattedMessage id={formErrors.phone} />
              </p>
            }
          </div>

          <div className="col-span-6 sm:col-span-6">
            <label htmlFor="message" className="block text-sm font-medium text-gray-700">
              <FormattedMessage id="form.message" />
              <span className="text-xs text-gray-400 ml-1">
                <FormattedMessage id="form.optional" />
              </span>
            </label>
            <textarea
              className="h-20 p-2 mt-3 border border-neutral-200 w-full sm:text-sm rounded-xl focus:ring-indigo-500 focus:border-indigo-500"
              type="textarea"
              name="message"
              placeholder={intl.formatMessage({ id: "form.message-placeholder" })}
              value={formValues.message}
              onChange={handleChange}
            />
          </div>

          <div className="col-span-6 sm:col-span-6">
            <button disabled={!isSubmit} className={`flex items-center px-8 py-3 border border-transparent text-base font-base rounded-xl text-white md:py-3 md:px-6 ${isSubmit ? "bg-blue-600 hover:bg-blue-700":"bg-slate-300 cursor-not-allowed"}`}>
              <FormattedMessage id="form.submit" />
            </button>
          </div>
        </div>
        <p className="text-center text-sm text-green-600">
          {formSubmit}
        </p>
      </div>
    </form>
  );
}

export default ContactForm;