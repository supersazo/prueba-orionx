import logoFull from '../assets/images/logo-full.svg';
import iconComingSoon from '../assets/images/icons/coming-soon.svg'
import iconExchange from '../assets/images/icons/exchange.svg'
import iconInstant from '../assets/images/icons/instant.svg'
import iconWallet from '../assets/images/icons/wallet.svg'
import { FormattedMessage } from 'react-intl';
import { forwardRef, useImperativeHandle } from 'react';

const Navbar = forwardRef((props, ref) => {

  useImperativeHandle(ref, () => ({
    closeProductOption: () => {
      let element = document.querySelector("#productOptions");
      let elementMobile = document.querySelector("#mobileNavbarOptions");
  
      if (!element.classList.contains("hidden")) {
        element.classList.add("hidden");
      }
      
      if (!elementMobile.classList.contains("hidden")) {
        elementMobile.classList.add("hidden");
      }
    }
  }));

  const handleProductsOption = () => {
    console.log('handleProductsOption');
    let element = document.querySelector("#productOptions");

    if (element.classList.contains("hidden")) {
      element.classList.remove("hidden");
    } else {
      element.classList.add("hidden");
    }
  }

  const handleMobileNavbar = () => {
    let element = document.querySelector("#mobileNavbarOptions");

    if (element.classList.contains("hidden")) {
      element.classList.remove("hidden");
    } else {
      element.classList.add("hidden");
    }
  }

  return (
    <div className="max-w-7xl mx-auto px-4 sm:px-6">
      <div className="relative pt-6 px-4 sm:px-6 lg:px-8">
        <nav className="relative flex items-center justify-between sm:h-10" aria-label="Global">
          <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
            <div className="flex items-center justify-between w-full md:w-auto">
              <img className="h-8 w-auto sm:h-10" src={logoFull} alt="orionx logo" />

              <div className="-mr-2 flex items-center md:hidden">
                <button onClick={handleMobileNavbar} type="button" className="rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 focus:outline-none" aria-expanded="false">
                  <span className="sr-only">Open main menu</span>
                  {/* Heroicon name: outline/menu */}
                  <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className="hidden md:flex md:ml-10 md:pr-4 md:space-x-8">
            <div className='relative'>
              
              <button type="button" onClick={handleProductsOption} className="text-gray-500 group rounded-md inline-flex items-center text-base font-medium hover:text-gray-900 focus:outline-none" aria-expanded="false">
                <span>
                  <FormattedMessage id="products" />
                </span>
                {/* Heroicon name: solid/chevron-down */}
                <svg className="text-gray-400 ml-2 h-5 w-5 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd" />
                </svg>
              </button>

              <div id='productOptions' className="hidden absolute z-10 -ml-4 mt-3 transform px-2 w-screen max-w-2xl sm:px-0 md:ml-0 md:left-1/2 md:-translate-x-1/2">
                <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                  <div className="relative grid grid-cols-2 gap-6 bg-white px-5 sm:gap-6 sm:p-6">
                    
                    <div className="-m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
                      <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconWallet} alt="icon wallet" />
                      
                      <div className="ml-4">
                        <p className="text-base font-medium text-gray-900">Wallet</p>
                        <p className="mt-1 text-sm text-gray-500">
                          <FormattedMessage id="nav.wallet-description" />
                        </p>
                      </div>
                    </div>

                    <div className="-m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
                      <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconExchange} alt="icon exhange" />

                      <div className="ml-4">
                        <p className="text-base font-medium text-gray-900">Exchange</p>
                        <p className="mt-1 text-sm text-gray-500">
                          <FormattedMessage id="nav.exchange-description" />
                        </p>
                      </div>
                    </div>

                    <div className="-m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
                      <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconInstant} alt="icon instant" />

                      <div className="ml-4">
                        <p className="text-base font-medium text-gray-900">Instant</p>
                        <p className="mt-1 text-sm text-gray-500">
                          <FormattedMessage id="nav.instant-description" />
                        </p>
                      </div>
                    </div>

                    <div className="-m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
                      <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconComingSoon} alt="icon coming soon" />

                      <div className="ml-4">
                        <p className="text-base font-medium text-gray-900">Coming Soon</p>
                        <p className="mt-1 text-sm text-gray-500">
                          <FormattedMessage id="nav.comingsoon-description" />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <a href="#mercado" className="font-medium text-gray-500 hover:text-gray-900">
              <FormattedMessage id="market" />
            </a>

            <a href="#soporte" className="font-medium text-gray-500 hover:text-gray-900">
              <FormattedMessage id="support" />
            </a>

            <a href="#blog" className="font-medium text-gray-500 hover:text-gray-900">
              <FormattedMessage id="blog" />
            </a>

            <a href="#ingresar" className="font-medium text-indigo-600 hover:text-indigo-500">
              <FormattedMessage id="enter" />
            </a>
          </div>
        </nav>
      </div>

      {/* Mobile menu, show/hide based on menu open state.*/}
      <div id="mobileNavbarOptions" className="hidden absolute z-10 top-0 inset-x-0 p-2 md:hidden">
        <div className="bg-white rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
          <div className="px-5 pt-4 flex w-full items-center justify-between">
            <img className="h-8 w-auto sm:h-10" src={logoFull} alt="" />

            <div className="-mr-2">
              <button type="button" onClick={handleMobileNavbar} className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Close main menu</span>
                {/* <!-- Heroicon name: outline/x --> */}
                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
            </div>
          </div>
          <div className="px-2 pt-2 pb-3 space-y-1">
            <div className="flex">
              <a href="#ingresar" className="block w-full px-5 py-3 m-2 text-center font-base rounded-xl text-white bg-blue-600 hover:bg-blue-700">
                <FormattedMessage id="enter" />
              </a>
              
              <a href="#ingresar" className="block w-full px-5 py-3 m-2 text-center font-base rounded-xl text-blue-600 bg-blue-200 hover:bg-blue-100">
                <FormattedMessage id="sign-up" />
              </a>
            </div>
            
            <a href="#productos" className="block px-3 py-2 rounded-md text-base cursor-default font-bold text-gray-700">
              <FormattedMessage id="products" />
            </a>

            <div className="m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
              <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconWallet} alt="icon wallet" />
              
              <div className="ml-4">
                <p className="text-base font-medium text-gray-900">Wallet</p>
                <p className="mt-1 text-sm text-gray-500">
                  <FormattedMessage id="nav.wallet-description" />
                </p>
              </div>
            </div>

            <div className="m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
              <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconExchange} alt="icon exhange" />

              <div className="ml-4">
                <p className="text-base font-medium text-gray-900">Exchange</p>
                <p className="mt-1 text-sm text-gray-500">
                  <FormattedMessage id="nav.exchange-description" />
                </p>
              </div>
            </div>

            <div className="m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
              <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconInstant} alt="icon instant" />

              <div className="ml-4">
                <p className="text-base font-medium text-gray-900">Instant</p>
                <p className="mt-1 text-sm text-gray-500">
                  <FormattedMessage id="nav.instant-description" />
                </p>
              </div>
            </div>

            <div className="m-3 p-3 flex items-start rounded-lg cursor-pointer hover:bg-gray-50">
              <img className="flex-shrink-0 h-6 w-6 text-indigo-600" src={iconComingSoon} alt="icon coming soon" />

              <div className="ml-4">
                <p className="text-base font-medium text-gray-900">Coming Soon</p>
                <p className="mt-1 text-sm text-gray-500">
                  <FormattedMessage id="nav.comingsoon-description" />
                </p>
              </div>
            </div>

            <a href="#mercado" className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50">
              <FormattedMessage id="market" />
            </a>

            <a href="#soporte" className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50">
              <FormattedMessage id="support" />
            </a>

            <a href="#blog" className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50">
              <FormattedMessage id="blog" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
});

export default Navbar;