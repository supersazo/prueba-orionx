import React, { useState, useEffect }  from 'react';
import NumberFormat from 'react-number-format';

import BTCLogo from '../assets/images/coins-logos/BTC.svg'
import DOTLogo from '../assets/images/coins-logos/DOT.svg'
import ETHLogo from '../assets/images/coins-logos/ETH.svg'
import USDTLogo from '../assets/images/coins-logos/USDT.svg'

function CoinPrices() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [prices, setPrices] = useState([]);

  const landingCoins = [
    'BTC',
    'ETH',
    'USDT',
    'DOT'
  ]

  // servicio no trae los nombres completos, en este arreglo hago match con el resto de la info necesaria
  const LandingCoinsFullNamesAndIcons = [
    {
      amountCurrencyCode: 'BTC',
      fullname: 'Bitcoin',
      icon: BTCLogo
    },
    {
      amountCurrencyCode: 'ETH',
      fullname: 'Ether',
      icon: ETHLogo
    },
    {
      amountCurrencyCode: 'USDT',
      fullname: 'Tether USD',
      icon: USDTLogo
    },
    {
      amountCurrencyCode: 'DOT',
      fullname: 'Polkadot',
      icon: DOTLogo
    },
  ]
  
  useEffect(() => {
    fetch("https://stats.orionx.com/ticker")
      .then(res => res.json())
      .then(
        (data) => {
          setIsLoaded(true);
          let resultArray = [];
          Object.entries(data).forEach(coin => {
            // busco solo las coins con priceCurrencyCod CLP
            if (coin[1].priceCurrencyCode === 'CLP') {
              // hago match solo con las expuestas en el diseño
              if (landingCoins.indexOf(coin[1].amountCurrencyCode) !== -1) {
                // obtengo nombre completo e icono que no son parte de la respuesta
                let nameAndIcon = LandingCoinsFullNamesAndIcons.filter(obj => {return obj.amountCurrencyCode === coin[1].amountCurrencyCode})[0]
                // junto todo en un solo json
                let result = {...nameAndIcon, ...coin[1]};
                resultArray.push(result);
              }
            }
          })
          setPrices(resultArray);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  });

  if (error) {
    return (
      <div className="flex flex-col gap-y-4 items-center m-auto md:gap-0 md:flex-row md:justify-evenly md:max-w-screen-lg text-red-600">
        Error: {error.message}
      </div>
    );
  } else if (!isLoaded) {
    return (
      <div className="flex flex-col gap-y-4 items-center m-auto md:gap-0 md:flex-row md:justify-evenly md:max-w-screen-lg">
        Cargando...
      </div>
    );
  } else {
    return (
      <div className="flex flex-col gap-y-4 items-center m-auto md:gap-0 md:flex-row md:justify-evenly md:max-w-screen-lg">
      {prices && prices.map(price => (
          <div className={`flex h-full p-2 rounded-lg border-2 w-11/12 md:w-56 ${price.amountCurrencyCode}`} key={price.name}>
            <img className="pr-2" src={price.icon} alt={price.fullname + 'logo'} />
            <div className="w-full">
              <p className="text-xs font-semibold">
                {price.fullname} <span className="font-light text-gray-600">{price.amountCurrencyCode}</span>
              </p>
              <p className="text-sm font-semibold">
                <NumberFormat value={price.lastPrice} displayType={'text'} thousandSeparator={true} prefix={'$'} />
              </p>
            </div>

            <div className={`font-semibold self-center ${price.variation24h > 0 ? "text-green-600":"text-red-600"}`}>
              {price.variation24h > 0 ? '+' + price.variation24h.toFixed(2):price.variation24h.toFixed(2)}%
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default CoinPrices;