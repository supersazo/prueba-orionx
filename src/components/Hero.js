import IphoneImage from '../assets/images/iphone.svg'
import ADA2Logo from '../assets/images/coins-logos/ADA2.svg'
import BTC2Logo from '../assets/images/coins-logos/BTC2.svg'
import DOT2Logo from '../assets/images/coins-logos/DOT2.svg'
import ETH2Logo from '../assets/images/coins-logos/ETH2.svg'
import SOL2Logo from '../assets/images/coins-logos/SOL2.svg'
import USDT2Logo from '../assets/images/coins-logos/USDT2.svg'
import { FormattedMessage } from 'react-intl';

function Hero() {
  return (
    <div className="mx-auto flex flex-col-reverse gap-2 items-center mt-20 p-6 md:flex-row">

      <div className="flex justify-center flex-col max-w-xl text-center md:ml-auto md:text-left">
        <h1 className="text-4xl font-bold">
          <FormattedMessage id="hero.bepartof" />
          <span className="block text-indigo-600"> <FormattedMessage id="hero.digital-revolution" /> </span>
          <FormattedMessage id="hero.and-start" />
        </h1>
        <p className="mt-4 text-lg text-gray-500">
          <FormattedMessage id="hero.subtitle" />
        </p>
        <div className="mt-4 justify-center sm:mt-8 sm:flex md:justify-start">
          <div className="rounded-md shadow">
            <a href="#crear-cuenta" className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-base rounded-xl text-white bg-blue-600 hover:bg-blue-700 md:py-3 md:px-6">
              <FormattedMessage id="create-account" />
            </a>
          </div>
        </div>
      </div>

      <div className="relative flex justify-center w-auto md:mr-auto ">
        <img className="w-auto h-auto" src={IphoneImage} alt='iphone' />
        
        <img className="absolute h-14 -left-4 -top-6" src={ADA2Logo} alt='ADA logo' />
        <img className="absolute h-12 left-6 bottom-72" src={USDT2Logo} alt='USDT logo' />
        <img className="absolute h-12 -left-6 bottom-16" src={ETH2Logo} alt='ETH logo' />

        <img className="absolute h-20 -right-8 top-14" src={BTC2Logo} alt='BTC logo' />
        <img className="absolute h-8 -right-8 top-2/4" src={DOT2Logo} alt='DOT logo' />
        <img className="absolute h-16 -right-6 bottom-24" src={SOL2Logo} alt='SOL logo' />
        
      </div>
    </div>
  );
}

export default Hero;