import React, {useContext} from 'react';
import { Context } from "./Wrapper";
import FacebookLogo from '../assets/images/social-logos/fb-logo.svg';
import DiscordLogo from '../assets/images/social-logos/discord-logo.svg';
import InstagramLogo from '../assets/images/social-logos/instagram-logo.svg';
import LinkedinLogo from '../assets/images/social-logos/linkedin-logo.svg';
import TwitterLogo from '../assets/images/social-logos/twitter-logo.svg';
import YoutubeLogo from '../assets/images/social-logos/youtube-logo.svg';
import TorusLogo from '../assets/images/Torus.svg';

import { FormattedMessage, useIntl } from 'react-intl';

function Footer() {
  const context = useContext(Context);
  const intl = useIntl();

  const handlerLanguageChange = (e) => {
    handleLanguageFlag(e);
    context.selectLanguage(e);
  }

  const handleLanguageFlag = (e) => {
    let element = e.target;

    if (element.value === 'en') {
      element.classList.add("bg-flag-en");
      element.classList.remove("bg-flag-es");
    } else {
      element.classList.add("bg-flag-es");
      element.classList.remove("bg-flag-en");
    }
  }

  return (
    <footer className="relative text-center lg:text-left bg-blue-900">
      <img className="absolute h-52 -right-20 -top-28 md:-right-0" src={TorusLogo} alt="torus logo" />
      <div className="w-11/12 md:max-w-5xl m-auto pt-24 text-left">
        <div className="grid grid-cols-2 gap-7 w-11/12  m-auto md:max-w md:gap-0 md:grid-cols-5">
          <div className="h-full md:w-52 col-span-1">
            <h6 className="uppercase font-semibold mb-4 text-white">
              <FormattedMessage id="footer.services" />
            </h6>
            <p className="mb-4">
              <a href="#!" className="text-white">Exchange</a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">Instant</a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">Wallet</a>
            </p>
            <p>
              <a href="#!" className="text-white">
                <FormattedMessage id="market" />
              </a>
            </p>
          </div>
          <div className="h-full md:w-52 col-span-1">
            <h6 className="uppercase font-semibold mb-4 md:justify-start">
              &nbsp;
            </h6>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.account" />
              </a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.security" />
              </a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.referrals" />
              </a>
            </p>
          </div>
          
          <div className="h-full col-span-2 md:w-52 md:col-span-1">
            <h6 className="uppercase font-semibold mb-4 text-white">
              <FormattedMessage id="footer.us" />
            </h6>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.enterprise" />
              </a>
            </p>
          </div>
          
          <div className="h-full col-span-2 md:w-52 md:col-span-1">
            <h6 className="uppercase font-semibold mb-4 text-white">
              <FormattedMessage id="footer.resources" />
            </h6>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="blog" />
              </a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.developers" />
              </a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="support" />
              </a>
            </p>
            <p className="mb-4">
              <a href="#!" className="text-white">
                <FormattedMessage id="footer.privacy-policy" />
              </a>
            </p>
          </div>
          
          <div className="grid grid-cols-2 gap-2 col-span-2 w-full h-fit md:w-48 md:col-span-1 justify-items-center">
            <div className="w-32 h-10 py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 m-auto" src={FacebookLogo} alt='facebook' />
            </div>
            <div className="w-32 h-10 py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 m-auto" src={DiscordLogo} alt='discord' />
            </div>
            <div className="w-32 h- py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 m-auto" src={TwitterLogo} alt='twitter' />
            </div>
            <div className="w-32 h-10 py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 m-auto" src={YoutubeLogo} alt='youtube' />
            </div>
            <div className="w-32 h-10 py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 m-auto" src={InstagramLogo} alt='instagram' />
            </div>
            <div className="flex w-32 h-10 py-1.5 px-6 cursor-pointer rounded-md bg-custom-social md:w-auto">
              <img className="h-7 p-1 m-auto" src={LinkedinLogo} alt='linkedin' />
            </div>
          </div>
        </div>
      </div>
      <div className="text-center p-12 pt-20">
        {/* Cambiar Idioma - select */}
        <select id="language" className="bg-flag-es h-7 w-32 text-right text-white rounded-md cursor-pointer bg-custom-social" value={context.locale} onChange={handlerLanguageChange}>
          <option value='es'>
            {intl.formatMessage({ id: "language.es" })}
          </option>
          <option value='en'>
            {intl.formatMessage({ id: "language.en" })}
          </option>
        </select>
      </div>
      <div className="text-center p-6 text-white">
        <span>©Orionx 2017 - 2022</span>
      </div>
    </footer>
  );
}

export default Footer;