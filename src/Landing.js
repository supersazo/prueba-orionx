// import logo from './logo.svg';
import Navbar from './components/Navbar';
import CoinPrices from './components/CoinPrices';
import ContactForm from './components/ContactForm';
import Hero from './components/Hero';
import Footer from './components/Footer';

import Star from './assets/images/Star.svg'

import { FormattedMessage } from 'react-intl';
import { useRef } from 'react';

function Landing() {
  const navbarRef = useRef();

  const handleMaindClick = () => {
    // close products options if are open
    navbarRef.current.closeProductOption();
  }

  return (
    <div className="relative overflow-hidden">
      <div className="bg-image hidden md:block"></div>
      <div className="bg-image-mobile block md:hidden"></div>
      
      <div className="relative">
        <header className="z-10">
          <Navbar ref={navbarRef} />
        </header>

        <section className="z-10" onClick={ handleMaindClick }>
          <Hero />
        </section>

        <section className="z-10">
          <CoinPrices />

          <div className="text-center p-6">
            <a className="text-sm text-blue-600 hover:text-blue-700" href="#ver-mercado">
              <FormattedMessage id="see-market" /> &#62;
            </a>
          </div>
        </section>

        <section className="relative z-10">
          <img className="absolute h-14 hidden left-20 top-12 md:block" src={Star} alt='Star' />
          <div className="flex flex-col max-w-lg m-auto p-6 justify-center text-center">
            <h1 className="text-3xl font-bold mb-2">
              <FormattedMessage id="join.title" /> &#223;eta <FormattedMessage id="join.title.2.spanish" />
            </h1>
            <p className='text-base'>
              <FormattedMessage id="join.subtitle" />
            </p>
          </div>
          
          <ContactForm />
        </section>

        <Footer />
      </div>
    </div>
  );
}

export default Landing;
