import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Landing from './Landing';
import reportWebVitals from './reportWebVitals';
import Wrapper from "./components/Wrapper";


ReactDOM.render(
  <Wrapper>
    <Landing />
  </Wrapper>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
